var {ipcRenderer} = require("electron")
window._module = window.module
window.module = null
delete window.module

ipcRenderer.sendToHost('requireParentPid')
ipcRenderer.sendToHost('loaded')
ipcRenderer.on('parentPid', function(e, pid) {
  console.log("Parent pid!")
  window.parentPID = pid
}.bind(this))

ipcRenderer.on('will-download', function(e, ev, url){
  ipcRenderer.sendToHost('will-download', url)
  console.log("Will download :3", url)
})

console.log(__dirname)
document.addEventListener("DOMContentLoaded", function() {
  var access = require(__dirname+'/../../DE/access.js')
  var $ = require('jquery')
  $(document).click(function() {
    console.log("Clicked")
    ipcRenderer.sendToHost('clickedOnTab')
  })

  document.body.addEventListener('contextmenu', function(e) {
    var menu_types = {
      'A': [
        {
          'type': 'normal',
          'name': 'Открыть в новой вкладке',
          'icon': '',
          callback: function() {
            window.open(elements[elements_n.indexOf('A')].href, '_blank')
          }.bind(this)
        },
        {
          'type': 'normal',
          'name': 'Копировать адрес ссылки',
          icon: '',
          callback: function() {var {clipboard} = require('electron'); clipboard.writeText(elements[elements_n.indexOf('A')].href)}
        }
      ],
      'IMG': [
        {
          'type': 'normal',
          'name': 'Копировать URL картинки',
          icon: '',
          callback: function() {var {clipboard} = require('electron'); clipboard.writeText(elements[elements_n.indexOf('IMG')].src)}
        }
      ]
    }
    var cur_menu = []
    var cur_menu_types = []
    var els = document.elementsFromPoint(e.clientX, e.clientY)
    console.log("ELS", els)
    elements = [...els]
    let i = 0
    while (i<els.length) {
      // console.log(el.nodeName, menu_types[el.nodeName])
      let el = els[i]
      elements = [...elements, ...$(el).parents()]
      i++
    }
    console.log('Elements:', elements)
    var elements_n = []
    i = 0
    while (i<elements.length) {
      // console.log(el.nodeName, menu_types[el.nodeName])
      let el = elements[i]
      elements_n.push(el.nodeName)
      if (menu_types[el.nodeName]!=undefined && cur_menu_types.indexOf(el.nodeName)<0) {
        cur_menu_types.push(el.nodeName)

        for (let i in menu_types[el.nodeName]) {
          console.log(i,menu_types[el.nodeName][i])
          cur_menu.push(menu_types[el.nodeName][i])
        }
      }
      i++
    }
    i = 0
    console.log(menu_types[e.target.nodeName], e.target.nodeName, cur_menu.reverse())
    // window.__contextMenuContext = this
    access.createContextMenu(cur_menu, window.parentPID, e.clientY + 80, e.clientX, true, this)
  }.bind(this))

  // let {ipcRenderer} = require('electron')

  $(document).on('mousemove', function(e){
    var els = document.elementsFromPoint(e.clientX, e.clientY)

    elements = [...els]
    let i = 0
    elements.map(function(el){
      elements = [...elements, ...$(el).parents()]
    })

    var elements_n = elements.map(function(el){
      return el.nodeName
    })

    if (elements_n.indexOf('A')>=0) {
      ipcRenderer.sendToHost('ahrefUnder', elements[elements_n.indexOf('A')].href)
    } else {
      ipcRenderer.sendToHost('noAhrefUnder', elements_n)
    }
  })
}.bind(this))

window.prompt = function(s){return s}

const $ = require('jquery')
require('jquery-circle-progress')
window.$ = $

function draw(img) {
  var canvas = document.createElement("canvas");
  var c = canvas.getContext('2d');
  c.width = canvas.width = img.width;
  c.height = canvas.height = img.height;
  c.clearRect(0, 0, c.width, c.height);
  c.drawImage(img, 0, 0, img.width, img.height);
  return c; // returns the context
}

// returns a map counting the frequency of colors
// in the image on the canvas
function getColors(c) {
  var col, colors = {};
  var pixels, r, g, b, a;
  r = g = b = a = 0;
  pixels = c.getImageData(0, 0, c.width, c.height);
  for (var i = 0, data = pixels.data; i < data.length; i += 4) {
    r = data[i];
    g = data[i + 1];
    b = data[i + 2];
    a = data[i + 3]; // alpha
    // skip pixels >50% transparent
    if (a < (255 / 2))
      continue;
    col = rgbToHex(r, g, b);
    if (!colors[col])
      colors[col] = 0;
    colors[col]++;
  }
  return colors;
}

function rgbToHex(r, g, b) {
  if (r > 255 || g > 255 || b > 255)
    throw "Invalid color component";
  return ((r << 16) | (g << 8) | b).toString(16);
}

// nicely formats hex values
function pad(hex) {
  return ("000000" + hex).slice(-6);
}

function LightenDarkenColor(col, amt) {

  var usePound = false;

  if (col[0] == "#") {
    col = col.slice(1);
    usePound = true;
  }

  var num = parseInt(col, 16);

  var r = (num >> 16) + amt;

  if (r > 255) r = 255;
  else if (r < 0) r = 0;

  var b = ((num >> 8) & 0x00FF) + amt;

  if (b > 255) b = 255;
  else if (b < 0) b = 0;

  var g = (num & 0x0000FF) + amt;

  if (g > 255) g = 255;
  else if (g < 0) g = 0;

  return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}

module.exports = {draw, getColors, rgbToHex, pad, LightenDarkenColor}
